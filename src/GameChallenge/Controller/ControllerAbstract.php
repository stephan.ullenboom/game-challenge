<?php
/**
 * Created by PhpStorm.
 * User: stephanullenboom
 * Date: 19.04.18
 * Time: 09:18
 */

namespace GameChallenge\Controller;


use GameChallenge\View\View;

abstract class ControllerAbstract
{
    /**
     * @var View
     */
    protected $view;
    protected $_request;
    protected $_files;
    protected $_renderScript;

    public function __construct($basePath, $option)
    {
        $this->view = new View($basePath);
        $this->_request = $option['request'];
        $this->_files = $option['files'];
        $this->_controller = strtolower($option['controller']);
        $this->_controllerName = $option['controllerName'];
        $this->_action = strtolower($option['action']);
        $this->_actionName = $option['actionName'];

        $this->resetRenderScript();
    }

    public function resetRenderScript()
    {
        $this->setRenderScript($this->_action, $this->_controller);
    }

    public function setRenderScript($action, $controller = null)
    {
        $controller = (is_null($controller) ? $this->_controller : $controller);
        $this->_renderScript = 'scripts'. DIRECTORY_SEPARATOR . $controller . DIRECTORY_SEPARATOR . $action;
    }

    public function render($reset = true)
    {
        $content = $this->view->render($this->_renderScript);
        if ($reset) {
            $this->resetRenderScript();
        }
        return $content;
    }
}