<?php
/**
 * Created by PhpStorm.
 * User: stephanullenboom
 * Date: 19.04.18
 * Time: 09:09
 */

namespace GameChallenge\Controller;


use GameChallenge\Models\Card;
use GameChallenge\Models\Deck;
use GameChallenge\Models\Gamer;

class GameCenter extends ControllerAbstract
{
    /**
     * @var array|\ArrayObject
     */
    protected $gamers = [];

    public function __construct($basePath, $option)
    {
        parent::__construct($basePath, $option);

        $this->gamers = new \ArrayObject();
        $this->gamers->append(new Gamer('Alice'));
        $this->gamers->append(new Gamer('Bob'));
        $this->gamers->append(new Gamer('Carol'));
    }

    public function indexAction()
    {
        $this->handOutCards();

        $values = [ 'gamers' => $this->gamers ];
        $this->view->setValues($values);
    }

    protected function handOutCards()
    {
        $counter = 0;
        while ($counter < 5) {
            /**
             * @var Gamer $gamer
             * @var Deck $deck
             */
            foreach ($this->gamers as $gamer) {
                $deck = $gamer->getDeck();
                do {
                    $card = new Card;

                } while ($deck->hasCard($card));

                $deck->addCard($card);
            }
            $counter++;
        };
    }
}