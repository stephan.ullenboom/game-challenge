<?php
/**
 * Created by PhpStorm.
 * User: stephanullenboom
 * Date: 19.04.18
 * Time: 09:36
 */

namespace GameChallenge\Models;


class Gamer
{
    protected $name;
    /**
     * @var Deck
     */
    protected $deck;

    /**
     * Gamer constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->addDeck(new Deck);
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Deck $deck
     */
    public function addDeck(Deck $deck)
    {
        $this->deck = $deck;
    }

    /**
     * @return mixed
     */
    public function getDeck()
    {
        return $this->deck;
    }

    public function toArray()
    {
        return [
            'name' => $this->name,
            'deck' => $this->deck->toArray(),
        ];
    }
}