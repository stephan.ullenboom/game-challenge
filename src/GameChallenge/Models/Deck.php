<?php
/**
 * Created by PhpStorm.
 * User: stephanullenboom
 * Date: 19.04.18
 * Time: 09:45
 */

namespace GameChallenge\Models;


class Deck
{
    protected $cards;

    public function __construct()
    {
        $this->cards = new \ArrayObject();
    }

    /**
     * @param Card $card
     */
    public function addCard(Card $card)
    {
        $this->cards->append($card);
    }

    /**
     * @param Card $card
     * @return bool
     */
    public function hasCard(Card $card)
    {
        /**
         * @var Card $deckCards
         */
        foreach ($this->cards as $deckCards) {
            if ($deckCards->getColor() === $card->getColor()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function hasWin()
    {
        return true;
    }

    public function toArray()
    {
        return [
            'cards' => $this->cards->getArrayCopy(),
        ];
    }
}