<?php
/**
 * Created by PhpStorm.
 * User: stephanullenboom
 * Date: 19.04.18
 * Time: 09:53
 */

namespace GameChallenge\Models;


class Card
{
    const WEISS = 0,
        BLUE = 1,
        GREEN = 2,
        RED = 3,
        YELLOW = 4,
        SCHWARZ = 5;

    protected $avaibleColors = [
        self::WEISS,
        self::BLUE,
        self::GREEN,
        self::RED,
        self::YELLOW,
        self::SCHWARZ,
    ];

    /**
     * @var integer $color
     */
    protected $color = null;

    /**
     * Card constructor.
     * @param null $color
     */
    public function __construct($color = null)
    {
        if (is_null($color)) {
            $this->color = rand(0, 6);
        }
    }

    public function getColor()
    {
        return $this->color;
    }
}