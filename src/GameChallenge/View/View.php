<?php
/**
 * Created by PhpStorm.
 * User: stephanullenboom
 * Date: 19.04.18
 * Time: 09:29
 */

namespace GameChallenge\View;


class View
{
    protected $version = 1.6;
    protected $_basePath;
    protected $_values = [];
    protected $_runtimeValues = [];

    public function __construct($basePath)
    {
        $this->_basePath = $basePath;
        $this->baseURl = '';
    }

    public function setValues(array $values)
    {
        foreach ($values as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function getValues()
    {
        return $this->_values;
    }

    public function __set($key, $value)
    {
        $this->_values[$key] = $value;
    }

    public function encode($value)
    {
        return utf8_encode($value);
    }

    public function __get($key)
    {
        if (isset($this->_values[$key])) {
            return $this->_values[$key];
        } else if (isset($this->_runtimeValues[$key])) {
            return $this->_runtimeValues[$key];
        }
        return false;
    }

    public function __isset($key)
    {
        return (isset($this->_values[$key]));
    }

    public function render($template, $values = [])
    {
        $this->_runtimeValues = $values;
        ob_start();
        $filePath = $this->_basePath . $template . '.phtml';
        if (!is_file($filePath)) {
            throw new \Exception('Template path not found! ' . $filePath);
        }
        include $filePath;
        $view = ob_get_clean();
        return $view;
    }

    public function __call($method, $args)
    {
        $mockMethods = ['basePath'];
        if (in_array($method, $mockMethods)) {
            return $args[0];
        }
        //echo 'Fatal error: Call to undefined method ' . $method;
        //die;
        //throw new \Exception('Fatal error: Call to undefined method ' . $method);
    }
}