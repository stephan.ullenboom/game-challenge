<?php
/**
 * Created by PhpStorm.
 * User: stephanullenboom
 * Date: 19.04.18
 * Time: 08:59
 */

namespace GameChallenge;

class Bootstrap
{
    protected $basePath;
    protected $options = [];
    static protected $rootPath = '';
    static protected $paths = [];

    protected $defaultController = 'GameCenter';
    protected $defaultAction = 'index';

    /**
     * Bootstrap constructor.
     * @param $rootPath
     * @throws \Exception
     */
    public function __construct($rootPath)
    {
        if (!is_dir($rootPath)) {
            throw new \Exception('Root path not found: ' . $rootPath);
        }

        date_default_timezone_set('Europe/Berlin');
        setlocale(LC_MONETARY, 'de_DE.utf8');

        $this->_setRootPath($rootPath);
        $this->_setBasePath($rootPath);
        $this->_registerAutoload();
        $this->_preDispatch();
    }

    protected function _setRootPath($rootPath)
    {
        self::$rootPath = $rootPath;
    }

    /**
     * @return string
     */
    public static function getRootPath()
    {
        return self::$rootPath . DIRECTORY_SEPARATOR;
    }

    protected function _setBasePath($rootPath)
    {
        $this->basePath = $rootPath . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . __NAMESPACE__;
        if (!is_dir($this->basePath)) {
            throw new \Exception('View path not found: ' . $this->basePath);
        }
    }

    protected function _registerAutoload()
    {
        if (empty(self::$paths)) {
            self::$paths = explode(PATH_SEPARATOR, get_include_path());
        }
        spl_autoload_register(array($this, '__autoload'));
    }

    protected function _preDispatch()
    {
        $controller = (isset($_REQUEST['controller']) ? $_REQUEST['controller'] : $this->defaultController);
        $action = (isset($_REQUEST['action']) ? $_REQUEST['action'] : $this->defaultAction);
        unset($_REQUEST['controller'], $_REQUEST['action']);
        $this->options = array(
            'controller' => $controller,
            'controllerName' => 'Controller\\' . $controller,
            'action' => $action,
            'actionName' => $action . 'Action',
            'request' => $_REQUEST,
            'files' => $_FILES,
        );
    }

    /**
     * Runs dispatch process
     */
    public function dispatch()
    {
        try {
            $controllerName = __NAMESPACE__ . '\\' . $this->options['controllerName'];
            $actionName = $this->options['actionName'];

            $viewPath = $this->basePath . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR;

            $controller = new $controllerName($viewPath, $this->options);
            $controller->$actionName();
            echo $controller->render();
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo '<pre>';
            echo $e->getTraceAsString();
            echo '</pre>';
        }
    }

    public function __autoload($className)
    {
        $file = str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
        foreach (self::$paths as $path) {
            $filePath = $path . DIRECTORY_SEPARATOR . $file;
            if (file_exists($filePath)) {
                require $filePath;
                return true;
            }
        }
        return false;
    }
}