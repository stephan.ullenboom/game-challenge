<?php
/**
 * Created by PhpStorm.
 * User: stephanullenboom
 * Date: 19.04.18
 * Time: 08:58
 */

error_reporting(E_ALL);
ini_set("display_errors", 1);

$rootPath = dirname(__FILE__);
$path = get_include_path()
    . $rootPath . DIRECTORY_SEPARATOR . "data" . PATH_SEPARATOR
    . $rootPath . DIRECTORY_SEPARATOR . "src" . PATH_SEPARATOR
    . $rootPath . DIRECTORY_SEPARATOR . "lib" . PATH_SEPARATOR;
set_include_path($path);

require_once $rootPath . "/src/Bootstrap.php";

use GameChallenge\Bootstrap;

$bootstrap = new Bootstrap($rootPath);
$bootstrap->dispatch();